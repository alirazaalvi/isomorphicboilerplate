# Isomorhpic Boilerplate
Download and install node and npm.

Install postgres sql. Update the server/config/database.json file and create database according to your settings.

## Prerequisites
```bash
$ npm install -g sequelize-cli
```
## Run
```bash
$ npm install
Navigate to server folder to run migrations.
$ sequelize db:migrate
$ npm start
```