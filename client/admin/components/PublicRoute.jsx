import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

class PublicRoute extends React.Component {
  render() {
    if (this.props.user.id || this.props.user.id > 1) {
      return <Redirect to="/" />;
    }

    const {component: Component} = this.props;
    return (
      <Route path={this.props.path} exact={this.props.exact} render={matchProps => (
        <div>
          <div className="container container-body">
            <Component {...matchProps} />
          </div>
        </div>
      )}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.userReducer.user,
  };
}

export default connect(mapStateToProps)(PublicRoute);