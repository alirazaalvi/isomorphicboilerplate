import React from 'react';
import { connect } from 'react-redux';
import Cookies from 'js-cookie';
import { getAdminUser } from '../actions/UserActions';
import Constants from '../constants';

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.dispatch = props.dispatch;
  }

  componentDidMount() {
    if (Cookies.get('token')) {
      this.dispatch(getAdminUser());
    } else {
      this.dispatch({
        type: Constants.APP_INITIALZED,
        data: {},
      });
    }
  }

  render() {
    return (
      <div>
        <div className="card">
          {this.props.children}
        </div>
      </div>);
  }
}

Main.propTypes = {
  dispatch: React.PropTypes.func,
  children: React.PropTypes.element,
};

function mapStateToProps(state) {
  return {
    reducers: state.mainReducer,
    user: state.userReducer.user,
    appInitializing: state.appReducer.appInitializing,
  };
}

export default connect(mapStateToProps)(Main);
