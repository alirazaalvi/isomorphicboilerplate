import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { resetUser } from '../../actions/UserActions';

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.dispatch = props.dispatch;
    this.signOut = this.signOut.bind(this);
  }

  signOut() {
    this.dispatch(resetUser());
  }

  render() {
    let userPublicLinksMarkup = [];
    if (this.props.user.id) {
      userPublicLinksMarkup = (
        <div id="navbar" className="collapse navbar-collapse">
          <ul className="nav navbar-nav">
            <li className="active"><Link to={'/'}>Home</Link></li>
            <li><Link to={'/users'}>Users</Link></li>
          </ul>
          <div className="pull-right">
            <ul className="nav navbar-nav">
              <li>
                <a onClick={this.signOut}>Log Out</a>
              </li>
            </ul>
          </div>
        </div>
        );
    }

    return (<nav className="navbar navbar-inverse navbar-fixed-top">
      <div className="container">
        <div className="navbar-header">
          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <a className="navbar-brand" href="#">Halogen</a>
        </div>
        {userPublicLinksMarkup}
      </div>
    </nav>
    );
  }
}

Menu.propTypes = {
  user: React.PropTypes.shape({
    id: React.PropTypes.number,
    firstName: React.PropTypes.string,
  }),
  dispatch: React.PropTypes.func,
};

function mapStateToProps(state) {
  return {
    user: state.userReducer.user,
  };
}

export default connect(mapStateToProps)(Menu);
