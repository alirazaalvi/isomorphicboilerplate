import React from 'react';
import { Field, reduxForm } from 'redux-form';
import InputControl from '../../../../common/formComponents/InputControl';

const emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

const validate = (values) => {
  const errors = {};

  if (values && !values.password) {
    errors.password = 'Required';
  }

  if (values && !values.email) {
    errors.email = 'Required';
  } else if (values && values.email && !emailRegEx.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  return errors;
};

const SignInForm = props => (
  <form onSubmit={props.handleSubmit}>
    <Field
      name="email"
      component={InputControl}
      type="text"
      label="Email"
      placeholder="Email"
    />
    
    <Field
      name="password"
      component={InputControl}
      type="password"
      label="Password"
      placeholder="Password"
    />
   
    <div className="pull-right">
      <button className="btn btn-primary" type="submit">Submit</button>
    </div>
    <div className="clearfix" />
  </form>
);

SignInForm.propTypes = {
  handleSubmit: React.PropTypes.func,
};

export default reduxForm({
  form: 'SignInForm',
  validate,
})(SignInForm);