import React from 'react';
import { Field, reduxForm } from 'redux-form';
import InputControl from '../../../../common/formComponents/InputControl';


const required = value => (value ? undefined : 'Required');

const EditUserForm = props => (
  <form onSubmit={props.handleSubmit}>
    <Field
      name="firstName"
      component={InputControl}
      type="text"
      label="First Name"
      placeholder="First Name"
      validate={[required]}
    />

    <Field
      name="lastName"
      component={InputControl}
      type="text"
      label="Last Name"
      placeholder="Last Name"
      validate={[required]}
    />
    <div className="pull-right">
      <button className="btn btn-primary" type="submit">Submit</button>
    </div>
    <div className="clearfix" />
  </form>
);

EditUserForm.propTypes = {
  handleSubmit: React.PropTypes.func,
};

export default reduxForm({
  form: 'SignUpForm',
  enableReinitialize: true,
})(EditUserForm);
