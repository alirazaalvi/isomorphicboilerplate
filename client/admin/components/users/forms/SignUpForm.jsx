import React from 'react';
import { Field, reduxForm } from 'redux-form';
import InputControl from '../../../../common/formComponents/InputControl';

const emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

const validate = (values) => {
  const errors = {};
  if (values && !values.confirmPassword) {
    errors.confirmPassword = 'Required';
  } else if (values && values.confirmPassword && values.confirmPassword !== values.password) {
    errors.confirmPassword = 'Password and confirm password mismatched.';
  }

  return errors;
};

const required = value => (value ? undefined : 'Required');
const emailRequired = value => (value && !emailRegEx.test(value) ? 'Invalid email address' : undefined);

const SignUpForm = props => (
  <form onSubmit={props.handleSubmit}>
    <Field
      name="firstName"
      component={InputControl}
      type="text"
      label="First Name"
      placeholder="First Name"
      validate={[required]}
    />

    <Field
      name="lastName"
      component={InputControl}
      type="text"
      label="Last Name"
      placeholder="Last Name"
      validate={[required]}
    />

    <Field
      name="email"
      component={InputControl}
      type="text"
      label="Email"
      placeholder="Email"
      validate={[required, emailRequired]}
    />

    <Field
      name="password"
      component={InputControl}
      type="password"
      label="Password"
      placeholder="Password"
      validate={[required]}
    />

    <Field
      name="confirmPassword"
      component={InputControl}
      type="password"
      label="Confirm Password"
      placeholder="Confirm Password"
    />

    <div className="pull-right">
      <button className="btn btn-primary" type="submit">Submit</button>
    </div>
    <div className="clearfix" />
  </form>
);

SignUpForm.propTypes = {
  handleSubmit: React.PropTypes.func,
};

export default reduxForm({
  form: 'SignUpForm',
  validate,
})(SignUpForm);
