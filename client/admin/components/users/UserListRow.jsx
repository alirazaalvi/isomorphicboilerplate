import React from 'react';
import { Link } from 'react-router-dom';

const UserListRow = props => (
  (
    <tr>
      <td>{`${props.user.firstName} ${props.user.lastName}`}</td>
      <td>{props.user.email}</td>
      <td>
        <Link to={`/edit-user/${props.user.id}`} className="btn btn-xs btn-default">
          Edit
        </Link>
      </td>
    </tr>
  )
);

UserListRow.propTypes = {
  user: React.PropTypes.shape({
    id: React.PropTypes.number,
    firstName: React.PropTypes.string,
    lastName: React.PropTypes.string,
    email: React.PropTypes.string,
  }),
};

export default UserListRow;
