import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import EditUserForm from './forms/EditUserForm';
import * as UserActions from '../../actions/UserActions';


class EditUser extends React.Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      user: {},
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.actions.getUser(id)
    .end((err, res) => {
      if (!err && res.ok) {
        this.setState({
          user: res.body.result,
        });
      }
    });
  }

  onSubmit(values) {
    if (values === null) {
      return;
    }
    this.props.actions.updateUser(values)
    .end((err, res) => {
      if (err || !res.ok) {
        const responseJson = res.body;
        if (responseJson && responseJson.message) {
          toastr.error(responseJson.message);
        }
      } else {
        toastr.success('Record updated successfully.');
      }
    });
  }


  render() {
    const initialValues = {
      initialValues: this.state.user,
    };

    return (<div>
      <h2>Sign In</h2>
      <EditUserForm
        onSubmit={this.onSubmit}
        user={this.state.user}
        {...initialValues}
      />
    </div>);
  }
}

EditUser.propTypes = {
  actions: React.PropTypes.shape({
    getUser: React.PropTypes.func,
    updateUser: React.PropTypes.func,
  }),
  params: React.PropTypes.shape({
    id: React.PropTypes.string,
  }),
};

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Object.assign({}, UserActions), dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditUser);
