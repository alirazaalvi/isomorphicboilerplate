import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUsers } from '../../actions/UserActions';
import UserListRow from './UserListRow';

class UsersList extends React.Component {
  constructor(props) {
    super(props);
    this.dispatch = props.dispatch;
  }

  componentDidMount() {
    this.dispatch(getUsers());
  }

  render() {
    const usersListMarkup = this.props.users.map(user => <UserListRow key={`user-${user.id}`} user={user} />);
    return (
      <table className="table table-condensed table-responsive table-sortable table-hover">
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {usersListMarkup}
        </tbody>
      </table>
    );
  }
}

UsersList.propTypes = {
  users: PropTypes.arrayOf(
    React.PropTypes.shape({}),
  ).isRequired,
  dispatch: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    users: state.userReducer.users,
  };
}

export default connect(mapStateToProps)(UsersList);
