import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import Menu from './layout/Menu';

class PrivateRoute extends React.Component {
  render() {
    if (this.props.appInitializing === true) {
      return (<div><img alt="spinner" src="/public/images/spinner.gif" /></div>);
    }
    
    if (!this.props.user.id || this.props.user.id < 1) {
      return <Redirect to="/login" />;
    }

    const {component: Component} = this.props;
    return (
      <Route path={this.props.path} exact={this.props.exact} render={matchProps => (
        <div>
          <Menu />
          <div className="container container-body">
            <Component {...matchProps} />
          </div>
        </div>
      )}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.userReducer.user,
    appInitializing: state.appReducer.appInitializing,
  };
}

export default connect(mapStateToProps)(PrivateRoute);