import immutable from 'seamless-immutable';
import Constants from '../constants/index';

const initialState = immutable({
  appInitializing: true,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case Constants.APP_INITIALZING:
      {
        return immutable.set(state, 'appInitializing', true);
      }
    case Constants.APP_INITIALZED:
      {
        return immutable.set(state, 'appInitializing', false);
      }
    default:
      {
        return state;
      }
  }
};
