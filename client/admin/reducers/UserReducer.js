import immutable from 'seamless-immutable';
import Constants from '../constants/index';

const initialState = immutable({
  user: {},
  users: [],
});

export default (state = initialState, action) => {
  switch (action.type) {
    case Constants.GET_USER_SUCCESS:
      return immutable.set(state, 'user', action.data);
    case Constants.GET_USERS_SUCCESS:
      return immutable.set(state, 'users', action.data);
    case Constants.RESET_USER:
      return immutable.set(state, 'user', {});
    default:
      return state;
  }
};
