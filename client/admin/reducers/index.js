import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import userReducer from '../reducers/UserReducer';
import appReducer from '../reducers/AppReducer';


export default combineReducers({
  userReducer,
  appReducer,
  form: formReducer,
});
