import React from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import { Provider } from 'react-redux';
import { 
  HashRouter as Router,
  Route,
  Switch,
  IndexRoute,
  hashHistory 
} from 'react-router-dom';
import Constants from './constants';
import Store from './stores/index';
import Home from './components/Home';
import SignIn from './components/users/SignIn';
import UsersList from './components/users/UsersList';
import EditUser from './components/users/EditUser';
import PrivateRoute from './components/PrivateRoute';
import PublicRoute from './components/PublicRoute';
import { getAdminUser } from './actions/UserActions';

if (Cookies.get('token')) {
  Store.dispatch(getAdminUser());
} else {
  Store.dispatch({
    type: Constants.APP_INITIALZED,
    data: {},
  });
}

ReactDOM.render(
  <Provider store={Store}>
    <Router history={hashHistory}>
      <Switch>
        <PrivateRoute exact path="/" component={Home} />
        <PublicRoute exact path="/login" component={SignIn} />
        <PrivateRoute exact path="/users" component={UsersList} />
        <PrivateRoute exact path="/edit-user/:id" component={EditUser} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('content'));
