import request from 'superagent';
import Cookies from 'js-cookie';
import Constants from '../constants/index';

export function getAdminUser() {
  return (dispatch) => {
    const token = Cookies.get('token');
    request.get('/admin/get-admin-user')
    .set('Accept', 'application/json')
    .set('Authorization', token)
    .end((err, res) => {
      if (!err && res.ok) {
        const responseJson = res.body;
        dispatch({
          type: Constants.GET_USER_SUCCESS,
          data: responseJson.result,
        });
      }

      dispatch({
        type: Constants.APP_INITIALZED,
        data: {},
      });
    });
  };
}

export function getUsers() {
  return (dispatch) => {
    const token = Cookies.get('token');
    request.get('/admin/get-users')
    .set('Accept', 'application/json')
    .set('Authorization', token)
    .end((err, res) => {
      if (!err && res.ok) {
        const responseJson = res.body;
        dispatch({
          type: Constants.GET_USERS_SUCCESS,
          data: responseJson.result,
        });
      }
    });
  };
}

export function updateUser(user) {
  return () => (
    request.post('/admin/update-user')
    .send(user)
    .set('Accept', 'application/json')
  );
}

export function getUser(id) {
  return () => (
    request.get(`/admin/get-user/${id}`)
    .set('Accept', 'application/json')
  );
}

export function resetUser() {
  return (dispatch) => {
    Cookies.remove('token');
    dispatch({
      type: Constants.RESET_USER,
      data: {},
    });
  };
}
