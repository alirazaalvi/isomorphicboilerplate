import React from 'react';

const InlineMessages = (props) => {
  const messagesList = props.messages.map((message, index) => <div key={`error-messages-${index}`}>{message}</div>);
  if (messagesList.length === 0) {
    return <span />;
  }
  return (<div className="bg-danger inline-error-messages">{messagesList}</div>);
};

InlineMessages.propTypes = {
  messages: React.PropTypes.arrayOf(React.PropTypes.string),
};

export default InlineMessages;
