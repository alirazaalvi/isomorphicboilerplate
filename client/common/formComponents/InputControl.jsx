import React from 'react';

const InputControl = ({ input, label, required, type, meta: { touched, error } }) => (
  <div className={`form-group ${touched && error ? 'has-error' : ''}`}>
    <label htmlFor={label} className="control-label">{label}</label>
    <div>
      <input
        {...input}
        className="form-control"
        placeholder={label}
        type={type}
        required={required}
      />
      {touched && ((error && <span className="help-block">{error}</span>))}
    </div>
  </div>
);

InputControl.propTypes = {
  label: React.PropTypes.string,
  type: React.PropTypes.string,
  required: React.PropTypes.bool,
  input: React.PropTypes.shape({}),
  meta: React.PropTypes.shape({}),
};

export default InputControl;
