export function stringHasValue(str) {
  if (!str || str.trim() === '' || str == null) {
    return false;
  }

  return true;
}
