import React from 'react';

const SignUpSuccess = () => (
  <div>
    <b>Your account has been created successfully. Please verify your email to login.</b>
  </div>
);

export default SignUpSuccess;
