import React from 'react';
import { connect } from 'react-redux';
import request from 'superagent';
import Cookies from 'js-cookie';
import SignInForm from './forms/SignInForm';
import { getUser } from '../../actions/UserActions';

class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.dispatch = props.dispatch;
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(values) {
    if (values === null) {
      return;
    }

    const postValues = values;
    postValues.userType = 2;
    request.post('/login')
    .send(postValues)
    .set('Accept', 'application/json')
    .end((err, res) => {
      if (err || !res.ok) {
        const responseJson = res.body;
        if (responseJson && responseJson.message) {
          toastr.error(responseJson.message);
        }
      } else {
        Cookies.set('token', res.body.token);
        this.dispatch(getUser());
        this.props.history.push('/');
      }
    });
  }


  render() {
    return (<div>
      <h2>Sign In</h2>
      <SignInForm
        onSubmit={this.onSubmit}
      />
    </div>);
  }
}

SignIn.propTypes = {
  dispatch: React.PropTypes.func,
  router: React.PropTypes.shape({
    push: React.PropTypes.func,
  }),
};

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(SignIn);
