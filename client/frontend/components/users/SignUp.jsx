import React from 'react';
import request from 'superagent';
import { connect } from 'react-redux';
import SignUpForm from './forms/SignUpForm';

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.dispatch = props.dispatch;
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(values) {
    if (values === null) {
      return;
    }
    request.post('/signup')
    .send(values)
    .set('Accept', 'application/json')
    .end((err, res) => {
      if (err || !res.ok) {
        const responseJson = res.body;
        if (responseJson && responseJson.message) {
          toastr.error(responseJson.message);
        }
      } else {
        this.props.router.push('/signup-success');
      }
    });
  }

  render() {
    return (<div>
      <h2>Sign Up</h2>
      <SignUpForm
        onSubmit={this.onSubmit}
      />
    </div>);
  }
}

SignUp.propTypes = {
  dispatch: React.PropTypes.func,
  router: React.PropTypes.shape({
    push: React.PropTypes.func,
  }),
};

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(SignUp);
