import React from 'react';

const LoggedInArea = () => (
  <h1>You are logged in!</h1>
);

export default LoggedInArea;
