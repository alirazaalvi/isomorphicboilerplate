import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Cookies from 'js-cookie';
import request from 'superagent';
import { resetUser } from '../actions/UserActions';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.dispatch = props.dispatch;
    this.checkAuth = this.checkAuth.bind(this);
    this.signOut = this.signOut.bind(this);
  }

  signOut() {
    this.dispatch(resetUser());
  }

  checkAuth() {
    const token = Cookies.get('token');
    request.get('/api')
    .set('Accept', 'application/json')
    .set('Authorization', token)
    .end((err, res) => {
      Cookies.remove('token');
    });
  }

  render() {
    let userPublicLinksMarkup = [];
    if (!this.props.user.id) {
      userPublicLinksMarkup = (
        <div>
          <b><Link to={'/signup'}>Sign Up</Link></b>
          <br />
          <b><Link to={'/login'}>Log In</Link></b><br />
        </div>
        );
    }
    return (<div>
      <h2>
        Homepage
        {this.props.user && this.props.user.id ? `(Welcome ${this.props.user.firstName})` : ''}
      </h2>
      <a onClick={this.checkAuth}>Check Auth</a><br />
      {this.props.user.id && <div><a onClick={this.signOut}>Signout</a></div>}
      {userPublicLinksMarkup}
      &nbsp;&nbsp;
    </div>);
  }
}

Home.propTypes = {
  user: React.PropTypes.shape({
    id: React.PropTypes.number,
    firstName: React.PropTypes.string,
  }),
  dispatch: React.PropTypes.func,
};

function mapStateToProps(state) {
  return {
    user: state.userReducer.user,
  };
}

export default connect(mapStateToProps)(Home);
