import request from 'superagent';
import axios from 'axios';
import Cookies from 'js-cookie';
import Constants from '../constants/index';

export function resetUser() {
  return (dispatch) => {
    Cookies.remove('token');
    dispatch({
      type: Constants.RESET_USER,
      data: {},
    });
  };
}

export function getUser() {
  return (dispatch) => {
    request.get('/get-user')
    .set('Accept', 'application/json')
    .end((err, res) => {
      if (!err && res.ok) {
        const responseJson = res.body;
        dispatch({
          type: Constants.GET_USER_SUCCESS,
          data: responseJson.result,
        });
      } else {
        dispatch(resetUser());
      }

      dispatch({
        type: Constants.APP_INITIALZED,
        data: {},
      });
    });
  };
}

export function* getUserPromise() {
  yield axios.get('/get-user');
}
