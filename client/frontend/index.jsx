import React from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import { Provider } from 'react-redux';
import { 
  HashRouter as Router,
  Route,
  Switch,
  IndexRoute,
  hashHistory 
} from 'react-router-dom';

import Constants from './constants';
import Store from './stores/index';
import Home from './components/Home';
import SignIn from './components/users/SignIn';
import SignUp from './components/users/SignUp';
import SignUpSuccess from './components/users/SignUpSuccess';
import LoggedInArea from './components/LoggedInArea';
import PrivateRoute from './components/PrivateRoute';
import PublicRoute from './components/PublicRoute';
import { getUser } from './actions/UserActions';

if (Cookies.get('token')) {
  Store.dispatch(getUser());
} else {
  Store.dispatch({
    type: Constants.APP_INITIALZED,
    data: {},
  });
}

ReactDOM.render(
  <Provider store={Store}>
    <Router history={hashHistory}>
      <Switch>
        <PrivateRoute exact path="/" component={Home} />
        <PublicRoute path="/login" component={SignIn} />
        <PublicRoute path="/signup" component={SignUp} />
        <PublicRoute path="/signup-success" component={SignUpSuccess} />
        <PrivateRoute path="/logged-in-area" component={LoggedInArea} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('content'));
