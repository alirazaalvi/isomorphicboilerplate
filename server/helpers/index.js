import _ from 'lodash';
import Boom from 'boom';

export function formatValidationErrorsList(validation) {
  const errors = [];
  if (validation) {
    _.forEach(validation.errors, (n) => {
      errors.push(n.message);
    });
  }

  return errors;
}

export function responseWithValidationErrorsList(errors) {
  const err = Boom.badRequest('Validation errors.');
  err.output.payload.message = errors;
  return err;
}
