import Crypto from 'crypto';
import Settings from '../config/settings';

export function strongEncrypt(value) {
  const salt = Settings.encryptionSalt;
  const iterations = Settings.encryptionIterations;
  const keyLength = Settings.encryptionKeyLength;
  const result = Crypto.pbkdf2Sync(value, salt, iterations, keyLength, 'sha512');
  return new Buffer(result).toString('base64');
}

export function basicHash() {
  const currentDate = (new Date()).valueOf().toString();
  const random = Math.random().toString();
  return Crypto.createHash('sha1').update(currentDate + random).digest('hex');
}

