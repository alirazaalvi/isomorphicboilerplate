import moment from 'moment';
import { strongEncrypt, basicHash } from '../helpers/Encryption';

export function checkLogin(db, email, password, userType) {
  return db.sequelize.query(
    'SELECT id, email, "userType" FROM users WHERE email = $email AND password = $password AND "userType" = $userType',
    {
      bind: {
        email,
        password: strongEncrypt(password),
        userType,
      },
      type: db.sequelize.QueryTypes.SELECT,
    });
}

export function getLoginTokenObject(user) {
  const loginTokenHash = basicHash();
  const loginToken = {
    token: loginTokenHash,
    expiration: moment().add(2, 'months').format(),
    id: user.id,
    email: user.email,
    userType: user.userType,
  };
  return loginToken;
}

export function fetchUser(db, email) {
  const sql = `SELECT id,
                "firstName", 
                "lastName", 
                email 
              FROM users 
              WHERE 
                email = $email
                AND "isEmailVerified" = $isEmailVerified
                AND "userType" = 2
                `;
  return db.sequelize.query(
    sql,
    {
      bind: {
        email,
        isEmailVerified: 1,
      },
      type: db.sequelize.QueryTypes.SELECT,
    },
  );
}

export function userExists(db, email) {
  return db.user.find({
    where: {
      email,
    },
  }).then(user => !!(user && user.email));
}
