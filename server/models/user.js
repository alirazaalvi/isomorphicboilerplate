module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    firstName: {
      type: DataTypes.STRING,
      field: 'firstName',
      allowNull: false,
      validate: { min: 3, max: 100, notEmpty: { msg: 'First name is required.' } },
    },
    lastName: {
      type: DataTypes.STRING,
      field: 'lastName',
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      field: 'email',
      validate: { isEmail: { msg: 'Email is not valid.' } }
    },
    isEmailVerified: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      field: 'isEmailVerified',
      allowNull: true,
    },
    password: {
      type: DataTypes.STRING,
      field: 'password',
      allowNull: false,
      validate:
      {
        notEmpty: { msg: 'Password is required.' },
      },
    },
    userType: {
      type: DataTypes.INTEGER,
      field: 'userType',
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    updatedAt: 'updatedAt',
    createdAt: 'createdAt',
  },
  );
  return User;
};
