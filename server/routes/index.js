import Path from 'path';
import adminRoutes from './adminRoutes';
import clientRoutes from './clientRoutes';

const indexRoutes = [{
  method: '*',
  path: '/{p*}',
  handler: (request, reply) => {
    reply.view('404').code(404);
  },
}, {
  method: 'GET',
  path: '/public/{param*}',
  config: { auth: false },
  handler: {
    directory: {
      path: Path.join(__dirname, '..', 'assets/'),
      redirectToSlash: true,
      index: true,
    },
  },
}];

const routes = indexRoutes
                .concat(clientRoutes)
                .concat(adminRoutes)
export default routes;
