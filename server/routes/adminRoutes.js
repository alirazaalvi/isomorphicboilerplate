import AdminController from '../controllers/admin/AdminController';
import AdminUserController from '../controllers/admin/AdminUserController';

export default [{
  method: 'GET',
  path: '/admin',
  config: { auth: false },
  handler: (request, reply) => {
    const adminController = new AdminController(request, reply);
    adminController.returnView();
  },
},
{
  method: 'GET',
  path: '/admin/get-admin-user',
  config: { auth: 'jwt_admin' },
  handler: (request, reply) => {
    const userController = new AdminUserController(request, reply);
    userController.getAdminUser();
  },
},
{
  method: 'GET',
  path: '/admin/get-user/{user}',
  config: { auth: 'jwt_admin' },
  handler: (request, reply) => {
    const adminUserController = new AdminUserController(request, reply);
    adminUserController.getUser();
  },
},
{
  method: 'GET',
  path: '/admin/get-users',
  config: { auth: 'jwt_admin' },
  handler: (request, reply) => {
    const adminUserController = new AdminUserController(request, reply);
    adminUserController.getUsers();
  },
},
{
  method: 'POST',
  path: '/admin/update-user',
  config: { auth: 'jwt_admin' },
  handler: (request, reply) => {
    const adminUserController = new AdminUserController(request, reply);
    adminUserController.updateUser();
  },
},
{
  method: 'POST',
  path: '/admin/login',
  config: { auth: false },
  handler: (request, reply) => {
    const adminUserController = new AdminUserController(request, reply);
    adminUserController.login();
  },
}];
