import MainController from '../controllers/MainController';
import UserController from '../controllers/UserController';


export default [{
  method: 'GET',
  path: '/',
  config: { auth: false },
  handler: (request, reply) => {
    const mainController = new MainController(request, reply);
    mainController.returnView();
  },
},
{
  method: 'POST',
  path: '/login',
  config: { auth: false },
  handler: (request, reply) => {
    const userController = new UserController(request, reply);
    userController.login();
  },
},
{
  method: 'POST',
  path: '/signup',
  config: { auth: false },
  handler: (request, reply) => {
    const userController = new UserController(request, reply);
    userController.signup();
  },
},
{
  method: 'GET',
  path: '/get-user',
  config: { auth: 'jwt' },
  handler: (request, reply) => {
    const userController = new UserController(request, reply);
    userController.getUser();
  },
}];
