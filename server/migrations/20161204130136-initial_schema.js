const Promise = require('promise'); 

module.exports = {
  up: (queryInterface, Sequelize) => {
    Promise.all([
      queryInterface.createTable('users',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
          },
          firstName: {
            type: Sequelize.STRING(100),
            allowNull: false,
          },
          lastName: {
            type: Sequelize.STRING(100),
          },
          email: {
            type: Sequelize.STRING,
            allowNull: false,
          },
          password: {
            type: Sequelize.STRING,
            allowNull: false,
          },
          userType: {
            type: Sequelize.INTEGER,
            allowNull: false,
          },
          isEmailVerified: {
            type: Sequelize.INTEGER,
          },
          createdAt: {
            type: Sequelize.DATE,
          },
          updatedAt: {
            type: Sequelize.DATE,
          },
        }),
    ]);
  },
  down: (queryInterface) => {
    queryInterface.dropTable('users');
  },
};
