export default class AdminController {
  constructor(request, reply) {
    this.request = request;
    this.reply = reply;
  }
  returnView() {
    const environment = process.env.NODE_ENV;
    const distBaseUrl = environment === 'production' ? 'public/js/dist/' : '../../';

    this.reply.view('admin', { distBaseUrl });
  }
}
