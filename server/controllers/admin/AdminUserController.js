import Boom from 'boom';
import _ from 'lodash';
import moment from 'moment';
import Jwt from 'jsonwebtoken';
import BaseController from '../BaseController';
import settings from '../../config/settings';
import { strongEncrypt, basicHash } from '../../helpers/Encryption';

export default class AdminUserController extends BaseController {
  login() {
    const userData = this.request.payload;
    const errors = [];
    const email = userData.email;
    const password = userData.password;
    let token = null;

    if (_.isEmpty(email) && _.isEmpty(password)) {
      errors.push('Invalid username or password');
      return this.reply(Boom.badRequest(errors));
    }

    return this.db.sequelize.query(
      'SELECT * FROM users WHERE email = $email AND password = $password AND "userType" = $userType',
      {
        bind: {
          email,
          password: strongEncrypt(password),
          userType: 1,
        },
        type: this.db.sequelize.QueryTypes.SELECT,
      },
      ).then((userRes) => {
        if (!userRes || userRes.length === 0) {
          errors.push('Invalid username or password.');
          return this.reply(Boom.badRequest(errors));
        }
        const user = userRes[0];
        const loginTokenObject = this.getLoginTokenObject(user);
        token = Jwt.sign(loginTokenObject,
        settings.encryptionSalt);
        return this.reply({ token });
      }).catch((err) => {
        console.log(err);
        errors.push('Invalid username or password');
        return this.reply(Boom.badRequest(errors));
      });
  }

  getAdminUser() {
    if (!this.request.auth || !this.request.auth.credentials.email) {
      return this.reply(Boom.badRequest());
    }
    const email = this.request.auth.credentials.email;
    return this.db.sequelize.query(
      'SELECT id, "firstName", "lastName", email FROM users WHERE email = $email',
      {
        bind: { email },
        type: this.db.sequelize.QueryTypes.SELECT,
      },
    )
    .then((user) => {
      if (user) {
        return this.reply({ result: user[0] }).header('Authorization', this.request.headers.authorization);
      }
      return this.reply(Boom.badRequest());
    });
  }

  getUser() {
    if (!this.request.params.user) {
      return this.reply(Boom.badRequest());
    }
    const id = this.request.params.user;
    return this.db.sequelize.query(
      'SELECT id, "firstName", "lastName", email FROM users WHERE id = $id',
      {
        bind: { id },
        type: this.db.sequelize.QueryTypes.SELECT,
      },
    )
    .then((user) => {
      if (user) {
        return this.reply({ result: user[0] }).header('Authorization', this.request.headers.authorization);
      }
      return this.reply(Boom.badRequest());
    });
  }

  getUsers() {
    return this.db.sequelize.query(
      'SELECT id, "firstName", "lastName", email FROM users WHERE "userType" = $userType',
      {
        bind: { userType: 2 },
        type: this.db.sequelize.QueryTypes.SELECT,
      },
    )
    .then((user) => {
      if (user) {
        return this.reply({ result: user });
      }
      return this.reply(Boom.badRequest());
    });
  }

  updateUser() {
    const userData = this.request.payload;
    const errors = [];

    const id = userData.id;
    const firstName = userData.firstName;
    const lastName = userData.lastName;

    if (!firstName || !lastName || !id) {
      errors.push('Please fill all fields.');
      return this.reply(Boom.badRequest(errors));
    }

    return this.db.sequelize.query(
      'UPDATE users set "firstName" = $firstName, "lastName" = $lastName WHERE id = $id',
      {
        bind: {
          id,
          firstName,
          lastName,
        },
        type: this.db.sequelize.QueryTypes.UPDATE,
      },
    )
    .then((result) => {
      if (result) {
        return this.reply({ result: {} });
      }
      return this.reply(Boom.badRequest());
    }).catch((error) => {
      console.log(error);
      errors.push('Unknown error.');
      return this.reply(Boom.badRequest(errors));
    });
  }

  getLoginTokenObject(user) {
    const loginTokenHash = basicHash();
    const loginToken = {
      token: loginTokenHash,
      expiration: moment().add(2, 'months').format(),
      id: user.id,
      email: user.email,
      userType: user.userType,
    };

    return loginToken;
  }
}
