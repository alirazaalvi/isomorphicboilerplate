import Boom from 'boom';
import _ from 'lodash';
import Jwt from 'jsonwebtoken';
import BaseController from './BaseController';
import settings from '../config/settings';
import { formatValidationErrorsList } from '../helpers';
import { strongEncrypt } from '../helpers/Encryption';
import { fetchUser
          , checkLogin
          , getLoginTokenObject
          , userExists,
      } from '../services/UserService';

export default class UserController extends BaseController {
  login() {
    const userData = this.request.payload;
    const errors = [];
    const { email, password, userType } = userData;
    let token = null;

    if (_.isEmpty(email) && _.isEmpty(password)) {
      errors.push('Invalid username or password');
      return this.reply(Boom.badRequest('', errors));
    }

    return checkLogin(this.db, email, password, userType)
      .then((userRes) => {
        if (!userRes || userRes.length === 0) {
          errors.push('Invalid username or password.');
          return this.reply(Boom.badRequest(errors));
        }
        const user = userRes[0];
        const loginTokenObject = getLoginTokenObject(user);
        token = Jwt.sign(loginTokenObject,
        settings.encryptionSalt);
        return this.reply({ token });
      }).catch((err) => {
        console.log(err);
        errors.push('Invalid username or password');
        return this.reply(Boom.badRequest(errors));
      });
  }

  async signup() {
    const userData = this.request.payload;
    let errors = [];
    const user = this.db.user.build(userData);
    user.userType = 2;
    // TODO: Remove the hard coding of email verification flag
    user.isEmailVerified = 1;
    const validation = await user.validate();
    errors = formatValidationErrorsList(validation);
    if (errors && errors.length > 0) {
      return this.reply(Boom.badRequest('', errors));
    }

    const doesUserExists = await userExists(this.db, user.email);
    if (doesUserExists) {
      errors.push('User already exists');
      return this.reply(Boom.badRequest(errors));
    }

    user.password = strongEncrypt(user.password);

    return user.save().then(() => {
      console.log('saved');
      return this.reply({ message: 'Account has been created successfully.' });
    }).catch(() => {
      console.log('error');
      errors.push('Unknown error');
      return this.reply(Boom.badRequest(errors));
    });
  }

  getUser() {
   if (!this.request.auth || !this.request.auth.credentials.email) {
      return this.reply(Boom.badRequest());
    }

    return fetchUser(this.db, this.request.auth.credentials.email)
      .then((user) => {
        if (user) {
          return this.reply({ result: user[0] });
        }
        return this.reply(Boom.badRequest());
      });
  }
}
