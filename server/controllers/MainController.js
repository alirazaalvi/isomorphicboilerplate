export default class MainController {
  constructor(request, reply) {
    this.request = request;
    this.reply = reply;
  }
  returnView() {
    const environment = process.env.NODE_ENV;
    const distBaseUrl = environment === 'production' ? 'public/js/dist/' : '../../';
    this.reply.view('index', { distBaseUrl });
  }

  returnJson() {
    this.reply({ result: 'success' }).header('Authorization', this.request.headers.authorization);
  }
}
