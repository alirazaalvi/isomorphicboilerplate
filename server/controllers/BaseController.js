export default class BaseController {
  constructor(request, reply) {
    this.request = request;
    this.reply = reply;
    this.db = this.request.server.app.db;
  }
}
