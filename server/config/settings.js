const settings = {
  encryptionSalt: 'T05O;{[H7uV#+yU',
  encryptionIterations: 4,
  encryptionKeyLength: 32,
  tempFolder: 'temp/',
  mediaFolder: 'media/',
};

export default settings;
