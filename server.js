import Hapi from 'hapi';
import Handlebars from 'handlebars';
import Vision from 'vision';
import Good from 'good';
import Inert from 'inert';
import HapiAauthJwt2 from 'hapi-auth-jwt2';
import webpackDev from 'webpack-dev-middleware';
import webpackHot from 'webpack-hot-middleware';
import hapiWebpackMiddleware from '@danielbayerlein/hapi-webpack-middleware';
import models from './server/models';
import routesIndex from './server/routes/index';
import extend from './server/views/helpers/handlebars-extend-blocks';
import webpack from './tasks/webpack.config.dev';
import settings from './server/config/settings';

// initializing sequelize
models.sequelize.sync();
// Create a server with a host and port
const server = new Hapi.Server();

server.connection({
  host: 'localhost',
  port: 8000,
});

// validation function
const validate = (decoded, request, callback) => {
  if (!decoded.id || !decoded.email || !decoded.userType) {
    return callback(null, false);
  }
  return models.user.find({
    where: {
      id: decoded.id,
      email: decoded.email,
      userType: decoded.userType,
    },
  })
  .then((user) => {
    if (user && user.email) {
      return callback(null, true);
    }
    return callback(null, false);
  });
};

const validateAdmin = (decoded, request, callback) => {
  if (!decoded.id || !decoded.email) {
    return callback(null, false);
  }
  return models.user.find({
    where: {
      id: decoded.id,
      email: decoded.email,
      userType: 1,
    },
  })
  .then((user) => {
    if (user && user.email) {
      return callback(null, true);
    }
    return callback(null, false);
  });
};

const goodConsoleOptions = {
  reporters: {
    console: [{
      module: 'good-console',
    }, 'stdout'],
  },
};

server.register([{
  register: Vision,
  options: {},
}, {
  register: Good,
  options: goodConsoleOptions,
}, {
  register: Inert,
  options: {},
}, {
  register: hapiWebpackMiddleware,
  options: {
    webpack: webpack,
    webpackDev: webpackDev,
    webpackHot: webpackHot,
  },
}, {
  register: HapiAauthJwt2,
  options: {},
}], (err) => {
  if (err) {
    throw err;
  }

  server.log('info', `Server running at: ${server.info.uri}`);
  server.auth.strategy('jwt', 'jwt',
    {
      key: settings.encryptionSalt,          // Never Share your secret key
      validateFunc: validate,            // validate function defined above
      verifyOptions: { algorithms: ['HS256'] }, // pick a strong algorithm
    });

  server.auth.strategy('jwt_admin', 'jwt',
    {
      key: settings.encryptionSalt,          // Never Share your secret key
      validateFunc: validateAdmin,            // validate function defined above
      verifyOptions: { algorithms: ['HS256'] }, // pick a strong algorithm
    });

  server.auth.default('jwt');
  server.app.db = models;
  server.route(routesIndex);
});

// setting the templating engine
server.views({
  engines: {
    html: extend(Handlebars),
  },
  relativeTo: __dirname,
  path: './server/views',
  layout: true,
  layoutPath: './server/views/layout',
  helpersPath: './server/views/helpers',
});

// Start the server
if (!module.parent) {
  server.start((err) => {
    if (err) {
      throw err;
    }
    console.log('Server running at:', server.info.uri);
  });
}

module.exports = server;
