import path from 'path';
import webpack from 'webpack';
import "babel-polyfill";


const sourcePath = path.join(__dirname, '../client');
const distPath = path.join(__dirname, '../server/assets/js/dist/');
const nodeEnv = 'production';

const plugins = [
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    minChunks: Infinity,
    filename: 'vendor.bundle.js',
  }),
  new webpack.DefinePlugin({
    'process.env': { NODE_ENV: JSON.stringify(nodeEnv) },
  }),
  new webpack.NamedModulesPlugin(),
];

plugins.push(
      new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: false,
      }),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false,
          screw_ie8: true,
          conditionals: true,
          unused: true,
          comparisons: true,
          sequences: true,
          dead_code: true,
          evaluate: true,
          if_return: true,
          join_vars: true,
        },
        output: {
          comments: false,
        },
      }),
    );

export default {
  devtool: 'source-map',
  context: sourcePath,
  entry: {
    vendor: ['react', 'seamless-immutable', 'react-dom', 'react-redux', 'redux', 'redux-thunk'],
    frontend: ['babel-polyfill', './frontend/index.jsx'],
    admin: ['babel-polyfill', './admin/index.jsx'],
  },
  target: 'web',
  output: {
    path: distPath,
    filename: '[name].js',
  },

  module: {
    rules: [
      {
        test: /\.html$/,
        exclude: /node_modules/,
        use: {
          loader: 'file-loader',
          query: {
            name: '[name].[ext]',
          },
        },
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    // modules: [
    //   path.resolve(__dirname, 'node_modules'),
    //   sourcePath,
    // ],
  },

  plugins,
  stats: {
    colors: {
      green: '\u001b[32m',
    },
  },


  devServer: {
    contentBase: './dist',
    historyApiFallback: true,
    port: 3000,
    compress: true,
    inline: false,
    hot: false,
    stats: {
      assets: true,
      children: false,
      chunks: false,
      hash: false,
      modules: false,
      publicPath: false,
      timings: true,
      version: false,
      warnings: true,
      colors: {
        green: '\u001b[32m',
      },
    },
  },
};
